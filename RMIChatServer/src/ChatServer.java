import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Vector;

public class ChatServer extends UnicastRemoteObject implements ChatServerInt {

	private Vector<ChatClientInt> v = new Vector<ChatClientInt>();

	public ChatServer() throws RemoteException {
	}

	public boolean login(ChatClientInt a) throws RemoteException {
		System.out.println(a.getName() + "  got connected....");
		boolean isDuplicate = false;
		for (ChatClientInt client : v) {
			if (a.getName().equals(client.getName())) {
				isDuplicate = true;
				break;
			}
		}

		if (!isDuplicate) {
			a.tell("You have Connected successfully.");
			publish(a.getName() + " has just connected.");
			v.add(a);
		} else {
			a.tell("Your Name is already used.");
		}

		return true;
	}

	public void publish(String s) throws RemoteException {
		System.out.println(s);
		for (int i = 0; i < v.size(); i++) {
			try {
				ChatClientInt tmp = (ChatClientInt) v.get(i);
				tmp.tell(s);
			} catch (Exception e) {
				// problem with the client not connected.
				// Better to remove it
			}
		}
	}

	public Vector getConnected() throws RemoteException {
		return null;
	}

	public boolean logout(ChatClientInt a) throws RemoteException {

		v.remove(a);

		a.tell("Your are logged out");
		publish(a.getName() + "logged out");

		return true;
	}
}